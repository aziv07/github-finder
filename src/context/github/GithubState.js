import { useReducer } from 'react'
import githubContext from './githubContext'
import GithubReducer from './githubReducer'
import {
  CLEAR_USERS,
  ALL_USERS,
  SEARCH_USERS,
  SET_LOADING,
  GET_USER,
  GET_REPOS,
} from '../types'
import axios from 'axios'
const GithubState = (props) => {
  let clientID
  let clientSecret
  if (process.env.NODE_ENV !== 'production') {
    clientID = process.env.REACT_APP_GITHUB_ID
    clientSecret = process.env.REACT_APP_SECRET
  } else {
    clientID = process.env.GITHUB_ID
    clientSecret = process.env.SECRET
  }
  const initialState = {
    users: [],
    user: {},
    repos: [],
    loading: false,
  }

  const [state, dispatch] = useReducer(GithubReducer, initialState)

  const find = async (txt) => {
    if (txt.length === 0) {
      // alerting()
    } else {
      setLoading()
      const { data } = await axios.get(
        `https://api.github.com/search/users?q=${txt}&client_id=${clientID}&client_secret=${clientSecret}`
      )
      if (data.items) {
        dispatch({ type: SEARCH_USERS, payload: data.items })
      }
    }
  }
  const reset = () => {
    setLoading()
    dispatch({ type: CLEAR_USERS })
    // setAlert(null)
  }

  const getUserRepos = async (name) => {
    setLoading()
    const { data } = await axios.get(
      `https://api.github.com/users/${name}/repos?per_page=5&sort=created:asc&client_id=${clientID}&client_secret=${clientSecret}`
    )
    if (data) {
      dispatch({ type: GET_REPOS, payload: data })
    }
  }

  const initial = async () => {
    setLoading()
    const { data } = await axios.get(
      `https://api.github.com/users?client_id=${clientID}&client_secret=${clientSecret}`
    )
    if (data) {
      dispatch({ type: ALL_USERS, payload: data })
    }
  }
  const getUser = async (name) => {
    setLoading()
    const { data } = await axios.get(
      `https://api.github.com/users/${name}?client_id=${clientID}&client_secret=${clientSecret}`
    )
    if (data) {
      dispatch({ type: GET_USER, payload: data })
    }
  }

  const setLoading = () => dispatch({ type: SET_LOADING })
  return (
    <githubContext.Provider
      value={{
        users: state.users,
        user: state.user,
        repos: state.repos,
        loading: state.loading,
        getUser,
        find,
        reset,
        getUserRepos,
      }}
    >
      {' '}
      {props.children}{' '}
    </githubContext.Provider>
  )
}

export default GithubState
