import {
  SET_EMPTY,
  ALL_USERS,
  SEARCH_USERS,
  SET_LOADING,
  GET_USER,
  CLEAR_USERS,
  GET_REPOS,
} from '../types'

export default (state, action) => {
  switch (action.type) {
    case SET_LOADING:
      return { ...state, loading: true }
    case SEARCH_USERS:
      return { ...state, users: action.payload, loading: false }
    case ALL_USERS:
      return { ...state, users: action.payload, loading: false }
    case SET_EMPTY:
      return { ...state, empty: false }
    case GET_USER:
      return { ...state, loading: false, user: action.payload }
    case CLEAR_USERS:
      return { ...state, loading: false, users: [] }
    case GET_REPOS:
      return { ...state, loading: false, repos: action.payload }
    default:
      return state
  }
}
