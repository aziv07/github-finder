import './App.css'
import React, { useEffect, useState } from 'react'
import Navbar from './components/Navbar'

import Alert from './components/Alert'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import About from './components/About'
import UserDetails from './components/UserDetails'
import GithubState from './context/github/GithubState'
import AlertState from './context/alert/alertState'
import Home from './components/Home'
import NotFound from './components/NotFound'

const App = () => {
  const [loading, setLoading] = useState(false)

  /* const init = async () => {
    setLoading()
    const { data } = await axios.get(
      `https://api.github.com/users?client_id=${process.env.REACT_APP_GITHUB_ID}&client_secret=${process.env.REACT_APP_SECRET}`
    )
    if (data) {
      dispatch({ type: ALL_USERS, payload: data })
    }
  }*/

  useEffect(() => {
    // console.log(context)
  }, [])

  const gachi = 'gachi the sinner'
  return (
    <GithubState>
      <AlertState>
        <Router>
          <div className='App'>
            <Navbar title={'GitHub Finder'} />
            <div className='container'>
              <Alert />
              <Switch>
                <Route path='/' exact component={Home} />
                <Route path='/about' component={About} />
                <Route
                  path='/user/:login'
                  render={(props) => <UserDetails {...props} />}
                />
                <Route component={NotFound} />
              </Switch>
            </div>
          </div>
        </Router>
      </AlertState>
    </GithubState>
  )
}

export default App
