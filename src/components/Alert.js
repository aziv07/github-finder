import React, { Fragment, useContext } from 'react'
import AlertContext from '../context/alert/alertContext'

function Alert() {
  const contxt = useContext(AlertContext)
  const { alert } = contxt
  return (
    <div>
      {alert !== null && (
        <div className={`alert alert-${alert.type}`}>
          <i className='fas fa-info-circle'></i> {alert.msg}
        </div>
      )}
    </div>
  )
}

export default Alert
