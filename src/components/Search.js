import React, { useContext, useState } from 'react'
import AlertContext from '../context/alert/alertContext'
import GithubContext from '../context/github/githubContext'

const Search = (props) => {
  const [name, setName] = useState('')
  const githubContext = useContext(GithubContext)
  const alertContext = useContext(AlertContext)
  const { setAlert } = alertContext
  const { reset } = githubContext
  const subFrom = (e) => {
    e.preventDefault()
    if (!name) {
      setAlert('Please Enter The name you want to find!', 'danger')
    } else {
      githubContext.find(name)
      setName('')
    }
  }
  return (
    <div>
      <form onSubmit={subFrom} className='form'>
        <input
          type='text'
          onChange={(e) => setName(e.target.value)}
          value={name}
          name='search'
          placeholder='Find User'
        />

        <input type='submit' value='Find' className='btn btn-dark btn-block' />
        {githubContext.users.length > 0 && (
          <button
            type='click'
            className='btn btn-light btn-block'
            onClick={reset}
          >
            Clear
          </button>
        )}
      </form>
    </div>
  )
}

export default Search
