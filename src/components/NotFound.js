import React from 'react'

function NotFound() {
  return (
    <div className='text-center'>
      <h1 className='text-danger'>Not Found</h1>
      <p className='lead'>Page Not found</p>
    </div>
  )
}

export default NotFound
