import React, { useContext } from 'react'
import GithubContext from '../context/github/githubContext'
import Spinner from './Spinner'
import User from './User'

const Users = () => {
  const gitContext = useContext(GithubContext)
  const { loading, users } = gitContext
  return (
    <div style={styling}>
      {loading ? (
        <div className='text-center'>
          <Spinner />
        </div>
      ) : (
        users.map((u) => <User key={u.id} user={u} />)
      )}
    </div>
  )
}

const styling = {
  display: 'grid',
  gridTemplateColumns: 'repeat(3, 1fr)',
  gridGap: '1rem',
}

export default Users
