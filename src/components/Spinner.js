import React, { Fragment } from 'react'
import spin from './spinner.gif'
const Spinner = () => (
  <Fragment>
    <img
      className='text-center'
      src={spin}
      alt='loading...'
      style={{ width: '200px', margin: 'auto', display: 'block' }}
    />
  </Fragment>
)

export default Spinner
