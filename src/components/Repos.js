import React, { useContext, useEffect } from 'react'
import GithubContext from '../context/github/githubContext'
import Repo from './Repo'
function Repos() {
  const contxt = useContext(GithubContext)

  return contxt.repos.map((r) => <Repo repo={r} key={r.id} />)
}

export default Repos
