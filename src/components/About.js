import React, { Fragment } from 'react'

function About() {
  return (
    <Fragment>
      <div className='text-center'>
        <h1>About this app</h1>
        <p>App to search Github users</p>
        <p>Version 1.0.0</p>
      </div>
    </Fragment>
  )
}

export default About
